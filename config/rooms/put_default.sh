#!/bin/sh

for N in *.json; do
  echo Uploading $N
  curl -X POST -H "Content-Type: application/json" -d @$N https://where.wealgo.org/api/rooms
done

