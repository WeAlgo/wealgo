# WeAlgo

Repo for https://where.wealgo.org. More information on https://wealgo.org

# Running

To run, you will need:

* The signalling server. In this repo at `/server`
* The web application. In this repo at `/site-dev`
* A web server to serve the web application. E.g. nginx.
* A TURN server to allow RTC peer connections over NAT. E.g. coturn

## Build Web Application

To build the web application, install node & npm and run:

    cd site-dev
    npm install
    npm run prod

## Run signalling server

The signalling server is a single node js application. Run:

    cd signal
    node room_service.js

Or install as a daemon

## Testing

The `testing` folder contains a basic selenium script. This can be used with custom video recording of a face to add users in an automated way.


## Configuration

Web application parameters are found in `site-dev/src/js/global.js`

Signalling server parameters are found in `signalserve/server.js`

## Contribute

Please help us with ideas and improvements at https://wealgo.org/engage


