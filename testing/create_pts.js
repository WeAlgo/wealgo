
let s = '';
for(var n=0; n < 68; n++) {

  let x=1;
  let y=2;
  while(true)
  {
    x = Math.random();
    y = Math.random();
    let dist = (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5);
    let max_radius = .25;
    if (dist < max_radius*max_radius) {
      break;
    }
  }

  s = s + '{x:' + x.toFixed(3) + ',y:' + y.toFixed(3) + '}, ';
  if (n%4 == 3) { s += '\n'; }
}
console.log(s);
