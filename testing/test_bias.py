import glob
import time
import sys
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

# files = glob.glob("bias_img/fairface/train/Black/_p0/*.jpg", recursive=True)
files = glob.glob("bias_img/fairface/train/Black/_p0/*.jpg")

# SFG: trying sorting
# for name in sorted(glob.glob('bias_img/fairface/train/Black/_p0/*.jpg')):
#     print(name)

n = '1'
if len(sys.argv) > 1:
    n = sys.argv[1]

options = Options()
options.add_argument("--disable-extensions")

#SFG: trying to solve crash
options.add_argument('--no-sandbox')

#options.add_argument("--use-fake-ui-for-media-stream");
#options.add_argument("--headless");
#options.add_argument("--use-fake-device-for-media-stream");
#options.add_argument("--use-file-for-fake-video-capture=./sample_Steve0"+n+".mjpeg");
#options.add_argument("--use-file-for-fake-audio-capture=./sample_Steve0"+n+".wav");

driver = webdriver.Chrome('./chromedriver', options=options)  # Optional argument, if not specified will search path.


# driver.get('https://where.wealgo.org/image-detect?enter')
driver.get('http://localhost:1234/image-detect?enter')
time.sleep(15) # Let the user actually see something!

# wait for element to appear, then hover it
for f in files:
    ff = os.getcwd() + '/' + f
    wait = WebDriverWait(driver, 20)

    inp = wait.until(ec.presence_of_element_located((By.XPATH, "//input[@type='file']")))
    inp.send_keys(ff)
    driver.find_element_by_xpath("//input[@type='submit']").click()
    result = wait.until(ec.text_to_be_present_in_element((By.ID, "result"), 'conf'))

    res = driver.find_element_by_id("result").text
    print(ff + '\t' + res)


driver.quit()
