import time
import sys
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

n = '1'
if len(sys.argv) > 1:
    n = sys.argv[1]

options = Options()
options.add_argument("--disable-extensions")
options.add_argument("--use-fake-ui-for-media-stream");
options.add_argument("--headless");
options.add_argument("--use-fake-device-for-media-stream");
# options.add_argument("--use-file-for-fake-video-capture=./sample_Ann0"+n+".mjpeg");
options.add_argument("--use-file-for-fake-video-capture=./sample_Ann01.mp4");

driver = webdriver.Chrome('./chromedriver', options=options)  # Optional argument, if not specified will search path.


driver.get('https://where.wealgo.org/')
# driver.get('http://localhost:1234/')
time.sleep(2) # Let the user actually see something!
driver.execute_script("enter('lobby')")
time.sleep(60) # Let the user actually see something!
# driver.execute_script("leave()")
driver.quit()
