import csv
import os


filename = 'bias_img/fairface/fairface_label_train.csv'

with open(filename, 'r') as csvfile:
    datareader = csv.reader(csvfile)
    for row in datareader:

        dirpath = os.path.join('bias_img/fairface/train', row[3])

        os.makedirs(dirpath, exist_ok=True)

        # os.rename("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
        os.rename(os.path.join("bias_img/fairface/train/", row[0]), os.path.join(dirpath, row[0]))

        # print(row[3])
