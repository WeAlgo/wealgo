const express = require("express");
const router = express.Router();

const defaultRoomJson = {"mods":["Rtc","FaceScan","RenderFaces","MaskEditor","PlaySounds","RoomTitle"],"doors":["lobby",null,null,null],"__v":0}
const Room = require("../models/Room");
//const indexRoomUsersData = require("../index.js");

// Get all rooms
router.get("/", async (req, res) => {
  try {
    const rooms = await Room.find().sort({ name: 1 });

    res.json(rooms);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/roomusers", async (req, res) => {
  try {
    // Get all rooms
    let rooms = await Room.find();

    rooms.forEach((room) => {
      if (global.rooms[room._id]) {
        room.users = global.rooms[room._id];
      }
    });
    res.json(rooms);
  } catch (err) {
    res.json({ message: err });
  }
});

// Get specific room
router.get("/:roomId", async (req, res) => {
  try {
    let room = await Room.findById(req.params.roomId);
    if(room === null) {
      const roomObj = defaultRoomJson;
      roomObj["_id"] = req.params.roomId;
      room = roomObj;
    }

    res.json(room);
  } catch (err) {
    res.json({ message: err });
  }
});

// Submit a new room
router.post("/", async (req, res) => {
  //let obj = JSON.parse(req.body);
  let obj = req.body;

  // Doesn't work: mongoose doesn't allow custom _id;
  // obj._id = encodeURIComponent(obj.title);

  const room = new Room(obj);
  try {
    const savedRoom = await room.save();
    res.json(savedRoom);
  } catch (err) {
    res.json({ message: err });
  }
});

// Update a room
router.patch("/:id", async (req, res) => {
  try {
    const updatedRoom = await Room.updateOne(
      { _id: req.params.roomId },
      { $set: { name: req.body.name } }
    );
    res.json(updatedRoom);
  } catch (err) {
    res.json({ message: err });
  }
});

// Delete specific room
router.delete("/:roomId", async (req, res) => {
  try {
    const removedRoom = await Room.deleteOne({ _id: req.params.roomId });
    res.json(removedRoom);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
