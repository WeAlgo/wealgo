const mongoose = require("mongoose");

const RoomSchema = mongoose.Schema({
  _id: {},
  mods: {},
  doors: {},
  title: {},
  users: {},
});

module.exports = mongoose.model("Rooms", RoomSchema);
