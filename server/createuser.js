const WebSocket = require('ws');

let ws = new WebSocket("ws://localhost:9888/foo");
ws.onmessage = (ev) => {
    //console.log("d", ev.data);

    let data = JSON.parse(ev.data);

    if (data.command !== "stats") {
      return;
    } else {
      //store_and_render(data.stats);
      this.wsData = data.rooms;
      console.log(" wsData: ", this.wsData);
      ws.close();
    }
  };