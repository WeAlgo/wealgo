const WebSocketServer = require("ws").Server;
const wss = new WebSocketServer({ port: process.env.port || 9888 });

// UID is just incremental number. We start at a random spot, to ensure no reuse
// after server restart
let nextUid = Math.floor(Math.random() * 10000);

const DEFAULT_ROOM_NAMES = [
  "lobby",
  "lobby2",
  "lobby3",
  "lobby4",
  "lobby5",
  "lobby6",
  "lobby7",
  "lobby8",
  "lobby9",
  "lobby10",
  "lobby11",
  "lobby12",
  "lobby13",
  "lobby14",
  "lobby15",
  "lobby16",
  "lobby17",
  "lobby18",
  "lobby19",
  "lobby20",
  "lobby21",
  "lobby22",
  "lobby23",
  "lobby24",
  "lobby25",
  "lobby26",
  "lobby27",
  "lobby28",
  "lobby29",
  "lobby30",
  "lobby31",
];

// Maximum before the next default is used
const MAX_USERS_DEFAULT_ROOM = 8;


// In memory object will contain a list of users for each room
global.rooms = {};

// ICE servers are used for P2P connections over NAT
const iceServers = [
  { urls: ["stun:stun2.l.google.com:19302"], username: "", credential: "" },
  {
    urls: ["turn:turn.wealgo.org:443?transport=tcp"],
    username: "wealgo",
    credential: "gy7c1lR588ZasP91gz",
  },
  {
    urls: ["turn:turn.wealgo.org:443?transport=udp"],
    username: "wealgo",
    credential: "gy7c1lR588ZasP91gz",
  },
];




// Incoming connection handler
wss.on("connection", (connection, request) => {
  // roomname = subpath
  let roomName = request.url.replace(/[?].*$/,'').replace(/^\//, "");

  if (!roomName) {
    roomName = getDefaultRoom();
  }

  // uid (user id) is incremental number
  const uid = nextUid;
  nextUid += 1;

  // helpers
  const respond = (message) => {
    connection.send(JSON.stringify(message));
  };

  const respondTo = (toUid, message) => {
    let user = room.find((user) => user.uid === toUid);
    if (user) {
      user.connection.send(JSON.stringify(message));
    }
    else {
      console.log("Cannot send", JSON.stringify(message), uid);
    }
  };

  // create room if it doesn't exist
  global.rooms[roomName] = global.rooms[roomName] || [];

  let room = global.rooms[roomName];

  // send existing users to me
  respond({
    command: "connect",
    roomName: roomName,
    iceServers: iceServers,
    users: room.filter(Boolean).map((user) => { return {
      uid: user.uid,
      is_viewer: user.is_viewer
    }; }),
    me: { uid: uid },
  });

  let me = {
    uid: uid,
    firstSeen: Date.now(),
    lastSeen: Date.now(),
    is_viewer: !!request.url.match(/[?&]view/),
    connection: connection,
  };

  // send me to existing users
  room.forEach((user) => {
    respondTo(user.uid, { command: "user-enter", user: {
      uid: me.uid,
      children: me.children,
      is_viewer: me.is_viewer
    }});
  });


  // add self to room
  room.push(me);

  console.log(
    new Date(),
    ";ROOM; 1;",
    roomName,
    ";",
    uid,
    ";",
    JSON.stringify(room.map((u) => u.uid))
  );

  // handle messages
  connection.on("message", (message) => {
    message = parseMessage(message);

    console.log("Incoming message: ", message);

    if (message) {
      switch (message.command) {
        case "ping": {
          me.lastSeen = Date.now();
          respond({ command: "pong" });
          break;
        }
        case "ice-candidate": {
          respondTo(message.uid, {
            command: "ice-candidate",
            uid: uid,
            candidate: message.candidate,
          });
          break;
        }
        case "sdp-offer": {
          respondTo(message.uid, {
            command: "sdp-offer",
            uid: uid,
            sdp: message.sdp,
          });
          break;
        }
        case "sdp-answer": {
          respondTo(message.uid, {
            command: "sdp-answer",
            uid: uid,
            sdp: message.sdp,
          });
          break;
        }
        case "send-metadata": {
          me.meta = message.metadata;
          respondTo(message.uid, {
            command: "user-metadata",
            uid: uid,
            metadata: message.metadata,
            pts: message.pts
          });
          break;
        }
        case "broadcast-metadata": {
          me.meta = message.metadata;
          room.forEach((user) => {
            if (user.uid !== uid) {
              respondTo(user.uid, {
                command: "user-metadata",
                uid: uid,
                metadata: message.metadata,
                pts: message.pts
              });
            }
          });
          console.log("GR:: ", global.rooms);
          break;
        }
        case "stats": {
          let replacer = (k, v) => {
            if (k !== "connection") return v;
          };
          connection.send(
            JSON.stringify({ command: "stats", rooms: global.rooms }, replacer)
          );
          break;
        }
        default: {
          // Default is to pass the message to user message.uid
          // Or broadcast if no message.uid
          // NOTE: Most of the above messages could be refactored to use this default
          const target_uid = message.uid;
          message.uid = uid;
          if (target_uid) {
            respondTo(target_uid, message);
          } else {
            room.forEach((user) => {
              if (user.uid !== uid) {
                respondTo(user.uid, message);
              }
            });
          }
        }
      }
    } else {
      respond(malformedMessage());
    }
  });

  connection.on("close", () => {
    let idx = room.findIndex((user) => user.uid == uid);
    if (idx === -1) {
      return;
    }
    for (let i = 0; i < room.length; i++) {
      if (i !== idx) {
        respondTo(room[i].uid, {
          command: "user-leave",
          uid: uid,
        });
      }
    }
    room.splice(idx, 1);
    console.log(
      new Date(),
      ";ROOM; -1;",
      roomName,
      ";",
      uid,
      ";",
      JSON.stringify(room.map((u) => u.uid))
    );
  });
});

function getDefaultRoom() {
  for (n = 0; n < DEFAULT_ROOM_NAMES.length; n++) {
    let r = DEFAULT_ROOM_NAMES[n];
    if (!global.rooms[r] || global.rooms[r].length < MAX_USERS_DEFAULT_ROOM) {
      return r;
    }
  }
  return "lobby_last";
}

parseMessage = (message) => {
  try {
    return JSON.parse(message);
  } catch (exception) {
    return undefined;
  }
};

malformedMessage = () => {
  return { error: "Malformed message" };
};

module.exports = global.rooms;
