const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv/config");

// Middlewares
app.use(cors()); // crossdomain stuff


app.use(bodyParser.json());
const roomsRoute = require("./routes/rooms");
// get request rooms

mongoose.connect(
  process.env.DB_CONNECTION,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  () => console.log("Connected to DB!")
);

app.use("/api/rooms", roomsRoute);

app.get("/", (req, res) => {
  res.send("Homepage");
});

module.exports = app;
