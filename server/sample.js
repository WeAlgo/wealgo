
// Connect & request stats

// Connect
let ws = new WebSocket('wss://where.wealgo.org/signalling/foo');

// Returns stats response. Note that this will include us in room foo
// as we are connected
ws.onmessage = (ev) => { console.log('d', ev.data)};

// Stats request on open
ws.onopen = () => { ws.send(JSON.stringify({command:"stats"})); };

