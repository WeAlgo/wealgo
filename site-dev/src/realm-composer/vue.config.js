module.exports = {
    css: {
        loaderOptions: {
            // Needs "sass": "^1.32.10" and sass-loader": "10.1.1" (not higher!)
            scss: {
                additionalData: `@import "~@/assets/scss/_global.scss";`,
            },
        },
    },
}
