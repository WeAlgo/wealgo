import axios from 'axios'
const api_url = 'http://localhost:3080/api/rooms/roomusers'


const state = {
    rooms: [],
}

const getters = {
   // allRoomsAdmin: (state) => state.rooms,
}

const actions = {
    async fetchRooms({ commit }, descending) {

        const response = await axios.get(api_url)
        descending
            ? commit('setRooms', response.data.reverse())
            : commit('setRooms', response.data)
    },


}

const mutations = {
    setRooms: (state, rooms) => (state.rooms = rooms),

}

export default {
    state,
    getters,
    actions,
    mutations,
}
