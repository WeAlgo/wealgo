import axios from 'axios'
const api_url = 'http://localhost:3080/api/rooms'

//const api_url_ursers = 'http://localhost:3080/api/rooms/roomusers'
//const api_url = 'https://where.wealgo.org/api/rooms'

const state = {
    rooms: [],
}

const getters = {
    allRooms: (state) => state.rooms,
}

const actions = {
    async fetchRooms({ commit }, descending) {
        const response = await axios.get(api_url)
        descending
            ? commit('setRooms', response.data.reverse())
            : commit('setRooms', response.data)
    },

    async deleteRoom({ commit }, id) {
        await axios.delete(api_url + `/${id}`)
        commit('removeRoom', id)
    },

    async createRoom({ commit }, room) {
        const response = await axios.post(api_url, room)
        commit('addRoom', response.data)
    },

    async updateRoom({ commit }, updatedRoom) {
        const response = await axios.put(
            api_url + `${updatedRoom.id}`,
            updatedRoom
        )
        commit('setUpdatedRoom', response.date)
    },
}

const mutations = {
    setRooms: (state, rooms) => (state.rooms = rooms),
    addRoom: (state, payload) => state.rooms.unshift(payload),
    removeRoom: (state, id) => {
        const index = state.rooms.findIndex((room) => room._id === id)
        state.rooms.splice(index, 1)
    },

    setUpdatedRoom: (state, updatedRoom) => {
        const index = state.rooms.findIndex(
            (room) => room.id === updatedRoom.id
        )
        if (index !== -1) {
            state.rooms.splice(index, 1, updatedRoom)
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations,
}
