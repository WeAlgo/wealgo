import { createStore } from 'vuex'
import rooms from '@/store/modules/rooms'
import admin from '@/store/modules/admin'

const state = {
    counter: 0,
}
const getters = {
    counter(state) {
        return state.counter
    },
}
const mutations = {
    SET_COUNTER(state, value) {
        state.counter = value
    },
}
const actions = {
    incrementCounter({ commit, state }, payload) {
        commit('SET_COUNTER', state.counter + payload)
    },
}
const modules = {
    rooms,
    admin
}

export default createStore({
    state,
    getters,
    mutations,
    actions,
    modules,
})
