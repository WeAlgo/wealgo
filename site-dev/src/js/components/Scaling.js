
import { Settings } from "../globals"




function move_using_nose(pos, me_size, room, landmarks) {
}
function getNewPosition(pos, me_size, room, face) {
  if (room.w === 0) {
    return pos;
  }

  // Determine screen bounds using face bounds.
  // First we mirror the face and calculate relative to center of face
  let b = {
    left: face.bounds.x + face.bounds.width - _width / 2,
    right: _width / 2 - face.bounds.x,
    top: _height / 2 - face.bounds.y,
    bottom: face.bounds.y + face.bounds.height - _height / 2,
  };

  // Translate these to [-1,1] coordinates
  let bounds = {
    left: (((b.left / _width) * me_size) / room.w) * 2 - 1,
    right: 1 - (((b.right / _width) * me_size) / room.w) * 2,
    top: (((b.top / _width) * me_size) / room.h) * 2 - 1,
    bottom: 1 - (((b.bottom / _width) * me_size) / room.h) * 2,
  };

  // find move rate by nose position
  const landmarks = face.landmarks;
  const noseX = landmarks[30].x;
  const noseY = landmarks[30].y;
  const noseExpectY = (landmarks[29].y*3 + landmarks[33].y*2) / 5;
  const noseExpectX = (landmarks[31].x + landmarks[35].x) / 2;
  const moveX = -(noseX - noseExpectX) / 2;
  const moveY = (noseY - noseExpectY) / 2;

  // Move and limit new position to bounds
  let newx = pos.x + moveX * Settings.MOVE_SPEED_X;
  let newy = pos.y + moveY * Settings.MOVE_SPEED_Y;
  pos.x = Math.min( bounds.right, Math.max(bounds.left, newx));
  pos.y = Math.min( bounds.bottom, Math.max(bounds.top, newy));

  // console.log(pos.x);
  // console.log(pos.y);
  let nx = pos.x - ((((landmarks[30].x - _width / 2 ) / _width) * me_size) / room.w) * 2 ;
  let ny = pos.y - ((((_height / 2 - landmarks[30].y) / _width) * me_size) / room.h) * 2 ;

  // Set door to 0-4 (0=None, 1=E, 2=N, 3=W, 4=S)
  if (ny > -.2 && ny < .2 && pos.x == bounds.right) {
    pos.door = 1;
  } else if (nx > -.2 && nx < .2 && pos.y == bounds.top) {
    pos.door = 2;
  } else if (ny > -.2 && ny < .2 && pos.x == bounds.left) {
    pos.door = 3;
  } else if (nx > -.2 && nx < .2 && pos.y == bounds.bottom) {
    pos.door = 4;
  } else {
    pos.door = 0;
  }


  return pos;

}


const _room = document.getElementById('_room');

// The size of the canvas is determined by the number of users and the room size
function get_user_canvas_size(user_count) {

  const size = Math.min(_room.offsetWidth, _room.offsetHeight);
  const user_size = (size / Math.pow(user_count, Settings.ZOOM_EXPONENT)) * Settings.ZOOM_FACTOR;
}

export { Position, move_using_nose }
