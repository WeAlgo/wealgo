// face regions, based on 68facial landmarks
// https://tastenkunst.github.io/brfv5-docs/assets/ui/brfv5_landmarks.jpg

import { Colors } from "./globals";

const Regions = [
  {
    label: "All",
    value: "all",
    perim: [0,17,18,19,20,23,24,25,26,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1],
    triang: [
      [42, 43, 47],
      [43, 44, 47],
      [44, 46, 47],
      [44, 45, 46],
      [36, 37, 41],
      [37, 40, 41],
      [37, 38, 40],
      [38, 39, 40],
      [29, 35, 30],
      [35, 34, 30],
      [34, 33, 30],
      [33, 32, 30],
      [32, 31, 30],
      [31, 29, 30],
      [28, 42, 29],
      [42, 47, 29],
      [47, 35, 29],
      [29, 31, 40],
      [29, 40, 39],
      [29, 39, 28],
      [48, 49, 60],
      [49, 61, 60],
      [49, 50, 61],
      [50, 62, 61],
      [50, 51, 62],
      [51, 52, 62],
      [52, 63, 62],
      [52, 53, 63],
      [53, 64, 63],
      [53, 54, 64],
      [48, 60, 59],
      [60, 67, 59],
      [67, 58, 59],
      [67, 66, 58],
      [66, 57, 58],
      [66, 56, 57],
      [66, 65, 56],
      [65, 55, 56],
      [65, 64, 55],
      [64, 54, 55],
      [22, 23, 42],
      [23, 42, 43],
      [23, 24, 43],
      [24, 44, 43],
      [24, 25, 44],
      [25, 45, 44],
      [25, 26, 45],
      [26, 16, 45],
      [0, 17, 36],
      [17, 18, 36],
      [18, 37, 36],
      [18, 19, 37],
      [19, 38, 37],
      [19, 20, 38],
      [20, 39, 38],
      [20, 21, 39],
      [20, 22, 21],
      [23, 22, 20],
      [22, 42, 27],
      [42, 28, 27],
      [21, 22, 27],
      [27, 28, 39],
      [27, 39, 21],
      [60, 61, 67],
      [61, 66, 67],
      [61, 62, 66],
      [62, 63, 66],
      [63, 65, 66],
      [63, 64, 65],
      [48, 31, 49],
      [31, 50, 49],
      [31, 32, 50],
      [32, 51, 50],
      [32, 33, 51],
      [33, 34, 51],
      [34, 51, 52],
      [34, 35, 52],
      [35, 52, 53],
      [35, 53, 54],
      [48, 6, 5],
      [48, 59, 6],
      [59, 7, 6],
      [59, 58, 7],
      [58, 8, 7],
      [56, 9, 8],
      [56, 55, 9],
      [55, 10, 9],
      [55, 54, 10],
      [54, 11, 10],
      [58, 57, 8],
      [57, 56, 8],
      [41, 31, 2],
      [41, 40, 31],
      [31, 3, 2],
      [31, 48, 3],
      [48, 4, 3],
      [48, 5, 4],
      [47, 46, 35],
      [46, 14, 35],
      [35, 14, 13],
      [35, 13, 54],
      [54, 13, 12],
      [54, 12, 11],
      [46, 15, 14],
      [46, 45, 15],
      [45, 16, 15],
      [0, 36, 1],
      [36, 41, 1],
      [41, 2, 1],
    ],
  },
  {
    label: "Eye right",
    value: "eyeRight",
    perim: [42, 43, 44, 45, 46, 47],
    dots: [42, 43, 44, 45, 46, 47],
    triang: [
      [42, 43, 47],
      [43, 44, 47],
      [44, 46, 47],
      [44, 45, 46],
    ],
  },

  {
    label: "Eye left",
    value: "eyeLeft",
    perim: [36, 37, 38, 39, 40, 41],
    dots: [36, 37, 38, 39, 40, 41],
    triang: [
      [36, 37, 41],
      [37, 40, 41],
      [37, 38, 40],
      [38, 39, 40],
    ],
  },

  {
    label: "Nose tip",
    value: "noseTip",
    perim: [29, 35, 34, 33, 32, 31],
    dots: [29, 35, 34, 33, 32, 31, 30],
    triang: [
      [29, 35, 30],
      [35, 34, 30],
      [34, 33, 30],
      [33, 32, 30],
      [32, 31, 30],
      [31, 29, 30],
    ],
  },
  {
    label: "Nose bridge",
    value: "noseBridge",
    perim: [40, 39, 28, 42, 47, 35, 29, 31],
    dots: [40, 39, 28, 42, 47, 35, 29, 31],
    triang: [
      [28, 42, 29],
      [42, 47, 29],
      [47, 35, 29],
      [29, 31, 40],
      [29, 40, 39],
      [29, 39, 28],
    ],
  },

  {
    label: "Lip upper",
    value: "lipUpper",
    perim: [48, 49, 50, 51, 52, 53, 54, 64, 63, 62, 61, 60],
    dots: [48, 49, 50, 51, 52, 53, 54, 64, 63, 62, 61, 60],
    triang: [
      [48, 49, 60],
      [49, 61, 60],
      [49, 50, 61],
      [50, 62, 61],
      [50, 51, 62],
      [51, 52, 62],
      [52, 63, 62],
      [52, 53, 63],
      [53, 64, 63],
      [53, 54, 64],
    ],
  },

  {
    label: "Lip lower",
    value: "lipLower",
    perim: [48, 60, 67, 66, 65, 64, 54, 55, 56, 57, 58, 59],
    dots: [48, 60, 67, 66, 65, 64, 54, 55, 56, 57, 58, 59],
    triang: [
      [48, 60, 59],
      [60, 67, 59],
      [67, 58, 59],
      [67, 66, 58],
      [66, 57, 58],
      [66, 56, 57],
      [66, 65, 56],
      [65, 55, 56],
      [65, 64, 55],
      [64, 54, 55],
    ],
  },

  {
    label: "Eye shadow right",
    value: "eyeShadowRight",
    perim: [22, 23, 24, 25, 26, 16, 45, 44, 43, 42],
    dots: [22, 23, 24, 25, 26, 16, 45, 44, 43, 42],
    triang: [
      [22, 23, 42],
      [23, 42, 43],
      [23, 24, 43],
      [24, 44, 43],
      [24, 25, 44],
      [25, 45, 44],
      [25, 26, 45],
      [26, 16, 45],
    ],
  },

  {
    label: "Eye shadow left",
    value: "LeftRight",
    perim: [0, 17, 18, 19, 20, 21, 39, 38, 37, 36],
    dots: [0, 17, 18, 19, 20, 21, 39, 38, 37, 36],
    triang: [
      [0, 17, 36],
      [17, 18, 36],
      [18, 37, 36],
      [18, 19, 37],
      [19, 38, 37],
      [19, 20, 38],
      [20, 39, 38],
      [20, 21, 39],
    ],
  },

  {
    label: "Frown",
    value: "frown",
    perim: [20, 23, 22, 42, 28, 39, 21],
    dots: [20, 23, 22, 42, 28, 39, 21, 27],
    triang: [
      [20, 22, 21],
      [23, 22, 20],
      [22, 42, 27],
      [42, 28, 27],
      [21, 22, 27],
      [27, 28, 39],
      [27, 39, 21],
    ],
  },

  {
    label: "Mouth hole",
    value: "mouthHole",
    perim: [60, 61, 62, 63, 64, 65, 66, 67],
    dots: [60, 61, 62, 63, 64, 65, 66, 67],
    triang: [
      [60, 61, 67],
      [61, 66, 67],
      [61, 62, 66],
      [62, 63, 66],
      [63, 65, 66],
      [63, 64, 65],
    ],
  },

  {
    label: "Moustache",
    value: "moustache",
    perim: [31, 32, 33, 34, 35, 54, 53, 52, 51, 50, 49, 48],
    dots: [31, 32, 33, 34, 35, 54, 53, 52, 51, 50, 49, 48],
    triang: [
      [48, 31, 49],
      [31, 50, 49],
      [31, 32, 50],
      [32, 51, 50],
      [32, 33, 51],
      [33, 34, 51],
      [34, 51, 52],
      [34, 35, 52],
      [35, 52, 53],
      [35, 53, 54],
    ],
  },

  {
    label: "Chin",
    value: "chin",
    perim: [48, 59, 58, 57, 56, 55, 54, 11, 10, 9, 8, 7, 6, 5],
    dots: [48, 59, 58, 57, 56, 55, 54, 11, 10, 9, 8, 7, 6, 5],
    triang: [
      [48, 6, 5],
      [48, 59, 6],
      [59, 7, 6],
      [59, 58, 7],
      [58, 8, 7],
      [56, 9, 8],
      [56, 55, 9],
      [55, 10, 9],
      [55, 54, 10],
      [54, 11, 10],
    ],
  },
  {
    label: "Chin tip",
    value: "chinTip",
    perim: [58, 57, 56, 8],
    dots: [58, 57, 56, 8],
    triang: [
      [58, 57, 8],
      [57, 56, 8],
    ],
  },

  {
    label: "Cheek left",
    value: "cheekLeft",
    perim: [41, 40, 31, 48, 5, 4, 3, 2],
    dots: [41, 40, 31, 48, 5, 4, 3, 2],
    triang: [
      [41, 31, 2],
      [41, 40, 31],
      [31, 3, 2],
      [31, 48, 3],
      [48, 4, 3],
      [48, 5, 4],
    ],
  },

  {
    label: "Cheek right",
    value: "cheekRight",
    perim: [47, 46, 14, 13, 12, 11, 54, 35],
    dots: [47, 46, 14, 13, 12, 11, 54, 35],
    triang: [
      [47, 46, 35],
      [46, 14, 35],
      [35, 14, 13],
      [35, 13, 54],
      [54, 13, 12],
      [54, 12, 11],
    ],
  },

  {
    label: "Blush right",
    value: "blushRight",
    perim: [46, 45, 16, 15, 14],
    dots: [46, 45, 16, 15, 14],
    triang: [
      [46, 15, 14],
      [46, 45, 15],
      [45, 16, 15],
    ],
  },

  {
    label: "Blush left",
    value: "blushLeft",
    perim: [0, 36, 41, 2, 1],
    dots: [0, 36, 41, 2, 1],
    triang: [
      [0, 36, 1],
      [36, 41, 1],
      [41, 2, 1],
    ],
  },
];

const DefaultRegionColoring =
// multiple (5) default masks
  [

    // // all blue dots, red left eye, blue mouth hole
    [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:5,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:6,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:10,fill:Colors.wealgoBlue,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoRed}],

    // all blue strokes, black fills, white left eye stroke & dots
    [{region:0,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:2,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:4,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:5,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:6,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:7,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:8,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:9,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:10,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:11,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:12,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:13,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:16,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:17,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:1,fill:Colors.wealgoBlack,stroke:Colors.wealgoWhite,dots:Colors.wealgoWhite},{region:15,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:14,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue}],


    // Deafult xmas man by Martine
    [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:5,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:2,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.wealgoWhite},{region:1,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoWhite},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.wealgoWhite,dots:Colors.wealgoWhite},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.wealgoWhite,dots:Colors.wealgoWhite},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.wealgoWhite,dots:Colors.wealgoWhite},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],


    // white jedi, red lips
    [{region:0,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:2,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:4,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:10,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:11,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:13,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:16,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:15,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:12,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:7,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:8,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:14,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:17,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:1,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:6,fill:Colors.wealgoRed,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue},{region:9,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.wealgoBlue},{region:5,fill:Colors.wealgoRed,stroke:Colors.wealgoWhite,dots:Colors.wealgoBlue}],


    // // red elephant, red velvet cookie
    // [{region:0,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlack,dots:Colors.wealgoBlue},{region:10,fill:Colors.wealgoBlack,stroke:Colors.wealgoBlack,dots:Colors.wealgoBlue},{region:4,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:3,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:11,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:17,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:8,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:7,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:16,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoBlue},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlue},{region:9,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.wealgoBlue},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoGreen},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.wealgoBlack,dots:Colors.wealgoWhite},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.wealgoBlack,dots:Colors.wealgoWhite},{region:6,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoPurple},{region:5,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.wealgoPurple}],
    //

    // tron by Ann Ellis
    //[{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:1,fill:Colors.wealgoRed,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoRed,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoRed,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.wealgoPurple,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.wealgoPurple,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.wealgoBlue,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.wealgoBlue,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],

    // // Tree
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoGreen,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoWhite},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoGreen,stroke:Colors.wealgoGreen,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Reindeer
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Santa Hat
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoWhite,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Angel Two
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:17,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:2,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:7,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:1,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:16,fill:Colors.wealgoPurple,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Kaars
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:9,fill:Colors.wealgoYellow,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent}],
    //
    // // Star One
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.wealgoYellow,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.wealgoYellow,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Star Two
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:5,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:1,fill:Colors.wealgoYellow,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.wealgoYellow,dots:Colors.colorMainTransparent}],
    //
    // // Maria
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:3,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoBlue,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoBlue,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoBlue,stroke:Colors.wealgoBlue,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // Jozef
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:3,fill:Colors.wealgoWhite,stroke:Colors.wealgoWhite,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent}],
    //
    // // Jezus Child
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:1,fill:Colors.wealgoWhite,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // King One
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoWhite,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoOrange,stroke:Colors.wealgoOrange,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoOrange,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent}],
    //
    // // King Two
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoRed,stroke:Colors.wealgoYellow,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoRed,stroke:Colors.wealgoRed,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoWhite,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent}],
    //
    // // King Three
    // [{region:0,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:9,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:12,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoPurple,stroke:Colors.wealgoPurple,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoBrown,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.wealgoPurple,stroke:Colors.wealgoYellow,dots:Colors.colorMainTransparent}],
    //
    // // Ezel
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoGrey,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoGrey,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoGrey,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoGrey,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoGrey,stroke:Colors.wealgoGrey,dots:Colors.colorMainTransparent},{region:9,fill:Colors.wealgoGrey,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack}],
    //
    // // Os
    // [{region:0,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:1,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:17,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:16,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:15,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:14,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:2,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:12,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:13,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:4,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.wealgoBlack},{region:8,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:7,fill:Colors.colorMainTransparent,stroke:Colors.colorMainTransparent,dots:Colors.colorMainTransparent},{region:3,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:11,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:5,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:10,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:6,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.colorMainTransparent},{region:9,fill:Colors.wealgoBrown,stroke:Colors.wealgoBrown,dots:Colors.wealgoBlack}],

  ];



export { Regions, DefaultRegionColoring };
