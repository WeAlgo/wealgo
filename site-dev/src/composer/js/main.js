import Vue from "vue";
import Vuetify from "vuetify";
import VueMoment from "vue-moment";

import App from "./App.vue";
Vue.use(Vuetify);
Vue.use(VueMoment);
new Vue({
  vuetify: new Vuetify(),
  render: (h) => h(App),
}).$mount("#app");
