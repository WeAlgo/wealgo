<?php namespace ProcessWire;


/*
* FAQ Homepage
*
*/

	function organisation() {

		$page = wire('page');
		$description = wire('pages')->get('/')->seo_description;


		$organisation = array(

		 "@context" => "https://schema.org",
		 "@type" => "Organization",
		 "name" => "WeAlgo",
		 "legalName" => "WeALgo",
		 "url" => wire('pages')->get('/')->httpUrl,
		 "logo" => wire('pages')->get('/')->og_image->url,
		 "foundingDate" => "2020",
		 "founders" => [
			  [
			 	"@type" => "Person",
			 	"name" => "Martine Stig"
			 	],
			 	[
			 	"@type" => "Person",
			 	"name" => "Stef Kolman"
			 	],
				[
			 	"@type" => "Person",
			 	"name" => "Tomas van der Wansem"
			 	],
				[
			 	"@type" => "Person",
			 	"name" => "Roel Noback"
			 	]
		  ],
		 "address" => [
		 	"@type" => "PostalAddress",
		 	"streetAddress" => "Feike de Boerlaan",
		 	"addressLocality" => "Amsterdam",
		 	"addressRegion" => "Noord Holland",
		 	"postalCode" => "1019kv",
		 	"addressCountry" => "Netherlands"
		 	],
		 "contactPoint" => [
		 	"@type" => "ContactPoint",
		 	"contactType" => "contact point",
		 	"telephone" => "",
		 	"email" => "hello@wealgo.org"
		 	],
		 "sameAs" => [
		 "https://www.instagram.com/wherewealgo/",
		 "https://gitlab.com/WeAlgo/wealgo",
		 "https://t.me/wealgo",
		 ]
	 );

		return '<script type="application/ld+json">' . json_encode($organisation) . '</script>';

	};
// organisation




/*
* FAQ Homepage
*
*/
		function schemaFaq() {
			$page = wire('page');

			$faqs = wire('pages')->get('template=faq');

			if(wireCount($faqs->faq_repeater)) {

			$mainEntity = array();

			foreach($faqs->faq_repeater as $faq) {
				$mainEntity[] = [

					"@type" => "Question",
					"name" => $faq->headline,
					"acceptedAnswer" => [
						"@type" => "Answer",
						"text" => $faq->body_text
							]
						];
					}

			$schema_faq = array(

				"@context" => "https://schema.org",
				"@type" => "FAQPage",
				"mainEntity" => $mainEntity

			);

		return '<script type="application/ld+json">' . json_encode($schema_faq) . '</script>';

		}

	}
