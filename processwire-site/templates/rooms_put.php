<?php namespace ProcessWire;


/*
* test ajax
*
*/
if($config->ajax) {

// echo 'ajax';


  foreach($rooms as $room) {

    // foreach($work->images as $img) {

      $data[] = array (
        '_id' => $room->room_id,
        'title' => $room->title,
        'name' => $room->name,
        'doors' => explode(",", $room->doors),
        'mods' => explode(",", $room->mods),
        'type' => $room->room_type->title,
        'topic' => $room->room_topic,
        'privacy' => $room->room_privacy->title,
        'room_start' => $room->room_start,
        'room_stop' => $room->room_stop,
      );
    // }
  };

   echo json_encode($data);
   return $this->halt();

} else {


  include('./_header.php');
  include('./_menu.php');

?>

<div id='home'>
  <!-- <p>ajax output of room</p> -->
  <div id='js-rooms_put'>put rooms</div>
</div>



<?php }
