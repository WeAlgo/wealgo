<?php namespace ProcessWire;

/*
** shouldnt really be here, but still gives some info... dunno think wireHTTP
** dont like output other then the curl process ...
*
*/
echo "<div id='home'>rooms saved</div>";

$url1 = "https://where.wealgo.org/api/users";
$roomname = "lobby";
$url2 = "https://where.wealgo.org/api/users/$roomname";

$http = new WireHttp();
$url = "https://where.wealgo.org/api/rooms";
// $data = $http->getJSON($url);
// print_r($data);

// Get the contents of a URL
$json = $http->getJSON($url);
if($json !== false) {


/*
** If we wanna show output of the get
*
*/
// foreach($json as $d) {
//
//   if(is_array($d['_id'])) { echo ' <br>';
//   } else {
//     if(isset($d['_id'])) { echo 'id: ' . $d['_id'] . '<br>'; }
//   }
//
//   if(isset($d['title'])) { echo 'title: ' . $d['title'] . '<br>'; }
//
//   // do doors
//   if(isset($d['doors'])) {
//     echo 'doors: ';
//
//     // empty array
//     $doors= [];
//
//     foreach ($d['doors'] as $e) {
//        $doors[] = $e;
//       }
//       $_doors = implode(",", $doors);
//     }
//     echo $_doors;
//
//   echo '<br>';
//
//
//   if(isset($d['mods'])) {
//     echo 'mods: ';
//
//     // empty array
//     $mods = [];
//
//     foreach ($d['mods'] as $e) {
//        $mods[] = $e;
//       }
//       $_mods = implode(",", $mods);
//     }
//     echo $_mods;
//
//   echo '<br><br>';
//
// }



/*
** Save rooms to processwire pages
*
*/
foreach($json as $d) {

  // remove legacy rooms with _id array
  if(is_array($d['_id'])) {
    return;
  } else {

    // _id is string, use to see if page already exists ...
    $pagename = strtolower($d['_id']);
    $p = $pages->findOne("room_id=$pagename");

    // check if page exists
    if(!$p->id) {
      // echo 'page dont exist, go ahead, save page';
        $p = new Page();
        // $p->of(false);
        $p->template = 'room';
        $p->parent = wire('pages')->get('/rooms/');
        $p->name = $pagename;
        $p->room_id = $pagename; // double, but leave for now

        // not all legacy rooms have titles
        if(isset($d['title'])) {
          $p->title = $d['title'];
        } else {
          $p->title = $pagename;
        }

        // do doors
        if(isset($d['doors'])) {

          // empty array
          $doors= [];

          foreach ($d['doors'] as $e) {
             $doors[] = $e;
            }
            $_doors = implode(",", $doors);
          }
        $p->doors = $_doors;

        // do mods
        if(isset($d['mods'])) {

          // empty array
          $mods= [];

          foreach ($d['mods'] as $e) {
             $mods[] = $e;
            }
            $_mods = implode(",", $mods);
          }
        $p->mods = $_mods;

        $p->save();
        // $p->of(true);

    }
  }
}


} else {
  echo "HTTP request failed: " . $http->getError();
}
