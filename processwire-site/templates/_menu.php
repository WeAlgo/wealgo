<?php namespace ProcessWire; ?>

<!--
* menu
*
-->
<div id="menu">
  <div class="menu-wrapper">

    <nav class='js-nav'>
      <ul>

        <!-- set menu items from pages -->
        <?php foreach($menu as $item): ?>

          <!-- set active class to li -->
          <?php
          $class = '';
          if($item->children->has($page) || $item === $page) {
            $class = 'active';
          } // endif
          ?>

          <li class='l <?= $class ?>'><a href='<?= $item->url ?>'><?= $item->title ?></a></li>

        <?php endforeach; ?>

      </ul>

        <!-- set footer menu items from footer -->
        <ul class='footer'>
          <?php foreach($social as $item): ?>
            <li><a class='smll' href='<?= $item->sponsor_link ?>'><?= $item->headline ?></a></li>
          <?php endforeach; ?>
        </ul>

    </nav>
  </div>
  <!-- .menu-wrapper -->
</div>
<!-- #menu -->
