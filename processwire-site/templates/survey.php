<?php namespace ProcessWire;
include('./_header.php');
include('./_menu.php');
 ?>


<div id='home'>

  <div class='survey_wrapper'>

    <div class='survey_inner'>

      <!-- set intro text & button -->
      <div class='survey_intro'>

        <div class='l survey_text center p-bottom'><?= $page->body_text ?></div>

        <button class='survey_start'><?= $page->button_text ?></button>

      </div>

      <!-- set form -->
      <?php echo $form_survey; ?>

    </div>

  </div>

</div>
