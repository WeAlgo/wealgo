<?php namespace ProcessWire; ?>

  <div id='header'>

      <header class='nav-down'>

        <!-- hemberber -->
        <div class="hamburger-outer js-hamburger-toggle">
          <div class="hamburger-inner js-hamburger-inner"></div>
        </div>

        <!-- logotype -->
        <a href='/'>
          <div class="masthead">
              <h2>Wealgo</h2>
              <!-- <img src='<?php // echo $mission->images->first()->width(120)->url; ?>'> -->
          </div>
        </a>

        <!-- nightmode switch -->
        <div class='nightmode'>
          <label class="theme-switch" for="checkbox">
            <input type="checkbox" id="checkbox" />
            <span class='overlay nightmode__icon'></span>
          </label>
        </div>

      </header>

  </div> <!-- #header -->
