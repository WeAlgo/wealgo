<?php namespace ProcessWire;
  include('./_header.php');
  include('./_menu.php');
?>


<div id='home'>
  <h1><?= $page->title; ?></h1>


  <?php
  foreach($rooms as $item): ?>

    <h2>Title: <?= $item->title; ?></h2>
    <p>ID: <?= $item->room_id; ?></p>
    <p>Doors: <?= $item->doors; ?></p>
    <p>Mods: <?= $item->mods; ?></p>
    <p>Type: <?= $item->room_type->title; ?></p>
    <p>Privacy: <?= $item->room_privacy->title; ?></p>
    <p>Topic: <?= $item->room_topic; ?></p>
    <p>Programming: <?= $item->room_programming->title; ?></p>
    <p>Start/Stop: <span class='smll'><?= $item->room_start; ?> - <?= $item->room_stop; ?></span></p>

    <br />

  <?php endforeach; ?>


</div>
