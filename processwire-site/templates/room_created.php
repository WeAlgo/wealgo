<?php namespace ProcessWire;
  include('./_header.php');
  include('./_menu.php');


  if($input->post->room_type) {

    // Radio button values
    // $allowed_types = ['play', 'present'];
    /**
    * Only allow allowed values above, returns NULL if anything other
    * than the values in the above array
    */
    // $room_type = $sanitizer->option($input->post->room_type, $allowed_values);
    $room_type = $sanitizer->int($input->post->room_type);
    $room_name = $sanitizer->text($input->post->room_name);
    $room_topic = $sanitizer->text($input->post->room_topic);
    $room_privacy = $sanitizer->int($input->post->room_privacy);
    $room_programming = $sanitizer->int($input->post->room_programming);
    $room_start = $sanitizer->date($input->post->room_start);
    $room_stop = $sanitizer->date($input->post->room_stop);


    // create new room page
    $p = new Page();
    $p->template = "room";
    $p->parent = $pages->get('/rooms/');
    $p->of(false);
    $p->title = $room_name;
    $p->room_type = $room_type;
    $p->room_topic = $room_topic;
    $p->room_privacy = $room_privacy;
    $p->room_programming = $room_programming;
    $p->room_start = $room_start;
    $p->room_stop = $room_stop;
    $p->save();

    $p->room_id = $p->id;
    $p->save();
    $p->of(true);

    $room_id = $p->room_id;
    $room_name = $pages->get("$room_id")->name;

}

  // page creation succesful, wait a few seconds
  // before redirecting to created room
  if($room_id) {
    $url = "https://where.wealgo.org/$room_name";
    // header('Refresh: 5; URL=http://yoursite.com/page.php');
    header("Refresh: 3; URL=$url");

    // redirect to newly created room
    // $session->redirect($url);

    $out = '';
    $out .= "<div id='created'>
              <div class='created'>
                <p>Excellent; we're preparing your Room.<br />
                We'll take you there in just a second ...</p>
                </div>
              </div>";

    echo $out;

  } else {

    // oops erros, show message and try again
    // by creating new room.
    $out = '';
    $out .= "<div id='created'>
              <div class='created'>
                <p class='error'>We are sorry ... something went wrong.<br />
                Please <a href='/create/'>try again</a></p>
                </div>
              </div>";
    echo $out;
  }



?>
