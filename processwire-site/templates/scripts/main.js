// SFG: trick to allow 100vh on mobile
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
function setHeight() {
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty("--vh", `${vh}px`);
}
// end SFG
setHeight();
window.addEventListener("resize", setHeight);
window.addEventListener("orientationchange", setHeight);


$(document).ready(function() {

  function navigation() {

    const menuWrapper = document.querySelector(".menu-wrapper");
    const nav = document.querySelector(".js-nav");
    // const materialIcons = document.querySelector(".js-material-icons");
    // const level2 = document.querySelector(".level-2");
    const eToggle = document.querySelector(".js-hamburger-toggle");
    const eInner = document.querySelector(".js-hamburger-inner");

    if (typeof eToggle !== "undefined" && eToggle !== null) {
      eToggle.addEventListener("click", (evt) => {

        console.log('open');

        menuWrapper.classList.toggle("open");
        if (menuWrapper.classList.contains("open")) {
          eInner.classList.add("animate");
        } else {
          eInner.classList.remove("animate");
        }

      });
    }
  };


/*
* SHOW HIDE HEADER ON SCROLL
*
*/
  showHideHeader = function() {
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  let navbarHeight = $('header').outerHeight();

  $(window).scroll(function(event){
      didScroll = true;
  });

  setInterval(function() {
      if (didScroll) {
          hasScrolled();
          didScroll = false;
      }
  }, 250);

  function hasScrolled() {
      var st = $(this).scrollTop();

      // Make sure they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta)
          return;

      // If they scrolled down and are past the navbar, add class .nav-up.
      // This is necessary so you never see what is "behind" the navbar.
      if (st > lastScrollTop && st > navbarHeight){
          // Scroll Down
          $('header').removeClass('nav-down').addClass('nav-up');
      } else {
          // Scroll Up
          if(st + $(window).height() < $(document).height()) {
              $('header').removeClass('nav-up').addClass('nav-down');
          }
      }

      lastScrollTop = st;
    }
  }



// darkMode switching
darkMode = function() {

const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
let currentTheme = localStorage.getItem('theme');
const initVideoLight = document.querySelector(".js-init-video-light");
const initVideoDark = document.querySelector(".js-init-video-dark");

if (!currentTheme) {
    currentTheme = 'dark';

    // SFG: set which to play: light or dark
    if(initVideoDark && initVideoLight) {
      initVideoLight.style.display = 'none';
      initVideoDark.style.display = 'block';
    }
}

document.documentElement.setAttribute('data-theme', currentTheme);

if (currentTheme === 'light') {
    toggleSwitch.checked = true;

    // SFG: set light / dark video on init screen
    if(initVideoDark && initVideoLight) {
      initVideoLight.style.display = 'block';
      initVideoDark.style.display = 'none';
    }
}

if (currentTheme === "dark") {
  // SFG: set light / dark video on init screen
  if(initVideoDark && initVideoLight) {
    initVideoLight.style.display = 'none';
    initVideoDark.style.display = 'block';
  }
}

function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'light');
        localStorage.setItem('theme', 'light');
        currentTheme = 'light';
        // console.log('light background');
        // SFG: set light / dark video on init screen on switch
        if(initVideoDark && initVideoLight) {
          initVideoLight.style.display = 'block';
          initVideoDark.style.display = 'none';
        }
    }
    else { document.documentElement.setAttribute('data-theme', 'dark');
          localStorage.setItem('theme', 'dark');
          currentTheme = 'dark';
          // console.log('dark background');

          // SFG: set light / dark video on init screen on switch
          if(initVideoDark && initVideoLight) {
            initVideoLight.style.display = 'none';
            initVideoDark.style.display = 'block';
          }
    }
}

toggleSwitch.addEventListener('change', switchTheme, false);

}

// fade in body on load
fadeIn = function() {
  document.getElementById('home').classList.add('animate');
}

// dont show body unless class is loaded to avoid white flash before -var is loaded
onLoad = function() {
  document.body.classList.add("loaded");
}


// toggleMenu();
navigation();
setHeight();
showHideHeader();
darkMode();
fadeIn();
onLoad();
window.addEventListener('resize', setHeight);
window.addEventListener('orientationchange', setHeight);


});
