// darkMode switching
function darkMode() {

const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
let currentTheme = localStorage.getItem('theme');

const initVideoLight = document.querySelector(".js-init-video-light");
const initVideoDark = document.querySelector(".js-init-video-dark");

if (!currentTheme) {
    currentTheme = 'dark';

    // SFG: set which to play: light or dark
    initVideoLight.style.display = 'none';
    initVideoDark.style.display = 'block';
}

document.documentElement.setAttribute('data-theme', currentTheme);

if (currentTheme === 'light') {
    toggleSwitch.checked = true;

    // SFG: set light / dark video on init screen
    initVideoLight.style.display = 'block';
    initVideoDark.style.display = 'none';
}

if (currentTheme === "dark") {
  // SFG: set light / dark video on init screen
  initVideoLight.style.display = 'none';
  initVideoDark.style.display = 'block';
}

function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'light');
        localStorage.setItem('theme', 'light');
        currentTheme = 'light';
        // console.log('light background');
        // SFG: set light / dark video on init screen on switch
        initVideoLight.style.display = 'block';
        initVideoDark.style.display = 'none';
    }
    else { document.documentElement.setAttribute('data-theme', 'dark');
          localStorage.setItem('theme', 'dark');
          currentTheme = 'dark';
          // console.log('dark background');

          // SFG: set light / dark video on init screen on switch
          initVideoLight.style.display = 'none';
          initVideoDark.style.display = 'block';
    }
}


toggleSwitch.addEventListener('change', switchTheme, false);

}

darkMode();
