
let height = window.innerHeight;

const next = `
  <div class='input__ok'>OK √</div>
  <div class='input__enter xs'>press <strong>Enter ↵</strong></div>
`;


// get inputFields, add child element
let inputField = document.getElementsByClassName('Inputfield');

[...inputField].forEach((parent, i) => {
  const childElement = document.createElement('li');
  childElement.className = 'input__next';
  // childElement.innerHTML = `second ${i}`;
  // childElement.innerHTML = next + `${i}`;
  childElement.innerHTML = next;
  parent.appendChild(childElement);
});




// add question number & arrow before question
const arrow = `
  <svg height="10" width="11"><path d="M7.586 5L4.293 1.707 5.707.293 10.414 5 5.707 9.707 4.293 8.293z"></path><path d="M8 4v2H0V4z"></path></svg>
`;

let description = document.getElementsByClassName('description');
[...description].forEach((parent, i) => {
  const preElement = document.createElement('span');
  preElement.className = 'q_number';
  preElement.innerHTML = `${i + 1}` + arrow;
  parent.insertBefore(preElement, parent.childNodes[0]);
})


// prevent form from sending when hitting enter
// var form = document.getElementById("FormBuilder_survey");
// form.addEventListener("submit", function(e) {
//   e.preventDefault();
//   return false;
// });


 $(document).ready(function () {

   // prevent enter key sending form on focussed text inputs
   $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
     if(e.which == 13) {
       e.preventDefault();
       return false;
     }
   });


   // fade out survey_intro
   $('button.survey_start').click(function(){
     var ele = $('.survey_intro');
     $("html, body").animate({
          scrollTop: $(ele).offset().top
     }, 1000);
     $(ele).fadeOut(500);

     // set focus on first input
     $('#FormBuilder_survey :input:enabled:visible:first').focus();
   })

   // wrap container around Inputfield children for styling
   $( ".Inputfield" ).wrapInner( "<div class='input__container'></div>");

   // set height
   $('.input__container').css('height', height - 64);

   // add onclick attribute to submit button
   let submit = $('#FormBuilder_survey button[name="survey_submit"]');
   submit.click(function(){
     sendForm();
   });

   // get all input elements on page
   let listen = $('input[type="text"], input[type="radio"], input[type="checkbox"]');

   // bind on radio & checkbox
   $(listen).bind("change", function() {

     // remove / add class show on current li.input__next
     if($(this).attr('type') == 'checkbox') {
       $('li.input__next').removeClass('show');
       let nextBtn = $(this).closest('.input__container').find('.input__next');
       nextBtn.addClass('show');

       // scroll submit into view on last item
       nextBtn.click(function(){
         console.log('clicked');
         $("html, body").animate({
           scrollTop: $(submit).offset().top
         }, 500);
       })

       // hide enter button on portrait
       if(window.innerHeight > window.innerWidth) {
         $(this).closest('.Inputfield').find('.input__enter').css('display', 'none');
       }
     }

     if($(this).attr('type') == 'radio') {
     let nextInput = $(this).closest('.Inputfield').next();

     $("html, body").animate({
       scrollTop: $(nextInput).offset().top
     }, 500);
   }

   });


   // bind text inputs
   $(listen).bind("input", function() {

     // remove / add class show on current li.input__next
     if($(this).attr('type') == 'text') { // show only on text inputs
       $('li.input__next').removeClass('show');
       // $(this).closest('.input__container').find('.input__next').addClass('show');
       let _nextBtn = $(this).closest('.input__container').find('.input__next');
       _nextBtn.addClass('show');


       let _nextInput = $(this).closest('.Inputfield').next();
       if(_nextInput) {
       _nextBtn.click(function(){
         $("html, body").animate({
           scrollTop: $(_nextInput).offset().top
         }, 500);

         // set focus on next input element
         var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
         nextFocus.focus();

       });
      }

       // hide enter button on portrait
       if(window.innerHeight > window.innerWidth) {
         $(this).closest('.Inputfield').find('.input__enter').css('display', 'none');
       }
     }


     $(this).keydown(function(event){
       let keycode = (event.keyCode ? event.keyCode : event.which);
       if(keycode ==  13) {
         event.preventDefault();

         let next = $(this).closest('.Inputfield').next();

         $("html, body").animate({
           scrollTop: $(next).offset().top
         }, 500);

         // set focus on next input element
         var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
         nextFocus.focus();

         return false;
       }

     });


       // // on enter inside input type = text, scroll to next question
       // $(this).keydown(function(event){
       //   var keycode = (event.keyCode ? event.keyCode : event.which);
       //   if(keycode == '13'){
       //
       //     console.log('enter in landscape');
       //
       //       var ele = $(this).closest('.Inputfield').next();
       //
       //       $("html, body").animate({
       //            scrollTop: $(ele).offset().top
       //       }, 500);
       //
       //       console.log('scrolled to next input');
       //
       //       // set focus on next input element
       //       var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
       //       nextFocus.focus();
       //
       //       return false;
       //   }
       // });


     });


   // });


   // text input
   // listen.each(function(){
   //   $(this).bind("change", function() {
   //
   //     let next = $(this).closest('.Inputfield').next();
   //
   //     $("html, body").animate({
   //        scrollTop: $(next).offset().top
   //     }, 500);
   //
   //     // set focus on next input element
   //     // var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
   //     // nextFocus.focus();
   //
   //   });
   // });

   // // listen for change on inputs
   // listen.each(function(){
   //   $(this).on('change input', function(){
   //
   //     // remove / add class show on current li.input__next
   //     $('li.input__next').removeClass('show');
   //     $(this).closest('.input__container').find('.input__next').addClass('show');
   //
   //     // hide enter button on portrait
   //     if(window.innerHeight > window.innerWidth) {
   //       $(this).closest('.Inputfield').find('.input__enter').css('display', 'none');
   //     }
   //
   //       $(this).keydown(function(event){
   //         let keycode = (event.keyCode ? event.keyCode : event.which);
   //         if(keycode == '13') {
   //
   //           if(window.innerHeight < window.innerWidth) { // landscape
   //             console.log('enter in landscape');
   //
   //             let nextInput = $(this).closest('.Inputfield').next();
   //             nextInput.css('border', '1px solid #f00');
   //
   //             $("html, body").animate({
   //                  scrollTop: $(nextInput).offset().top
   //             }, 500);
   //
   //           } else { // portrait
   //             console.log('enter in portrait');
   //
   //           }
   //
   //           // set focus on next input element
   //           var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
   //           nextFocus.focus();
   //
   //           return false;
   //
   //         }
   //       })
   //
   //   })
   // })


}); // document ready

// find closest input__next and show, hide others
// let listen = $('input[type="text"], input[type="radio"], input[type="checkbox"]');
// listen.each(function(){
//
//   $(this).on('input change', function(e) {
//
//     $('li.input__next').removeClass('show');
//     $(this).closest('.input__container').find('li.input__next').addClass('show');
//   });
//
//
//   //check if viewport / device == portrait / landscape
//   if(window.innerHeight > window.innerWidth){
//
//     let ele = $(this).closest('.Inputfield').next();
//     let next = $(this).closest('.Inputfield').find('.input__ok').css('border', '1px solid #f90');
//     $(this).closest('.Inputfield').find('.input__enter').css('display', 'none');
//
//     //portrait
//     $(this).keydown(function(event){
//       let keycode = (event.keyCode ? event.keyCode : event.which);
//       if(keycode == '13'){
//         console.log('enter in portrait');
//         return false;
//       }
//
//
//       console.log($(this).attr('type'));
//
//       $(next).click(function(){
//
//         // console.log('next clicked');
//
//         $("html, body").animate({
//              scrollTop: $(ele).offset().top
//         }, 500);
//
//         // set focus on next input element
//         let nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
//         nextFocus.focus();
//
//       });
//
//     });
//
//   } else { // landscape
//
//     // on enter inside input type = text, scroll to next question
//     $(this).keydown(function(event){
//       var keycode = (event.keyCode ? event.keyCode : event.which);
//       if(keycode == '13'){
//
//         console.log('enter in landscape');
//
//           var ele = $(this).closest('.Inputfield').next();
//
//           $("html, body").animate({
//                scrollTop: $(ele).offset().top
//           }, 500);
//
//           console.log('scrolled to next input');
//
//           // set focus on next input element
//           var nextFocus = $(this).closest('.Inputfield').next().find('.InputfieldMaxWidth');
//           nextFocus.focus();
//
//           return false;
//       }
//     });
//
//   }
//
// });



function sendForm() {
  document.getElementById('FormBuilder_survey').submit();
}
