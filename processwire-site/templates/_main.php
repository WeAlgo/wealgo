<?php namespace ProcessWire; ?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

 <!-- custom browser element colors -->
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content='#161625'>
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content='#161625'>
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
 <!-- /custom browser element colors -->

  <!-- FAVICON -->
  <link rel="icon" type="image/x-icon" href="<?php echo $config->urls->assets?>favicons/favicon.ico?v=2">
  <link rel="apple-touch-icon" sizes="128x128" href="<?php echo $config->urls->assets?>favicons/apple-touch-icon-128.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $config->urls->assets?>favicons/apple-touch-icon-180.png">
  <!-- !FAVICON -->


<!-- STYLE -->
  <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
<!-- /STYLE -->


<!-- forms -->
<?php echo $form_create->styles; ?>
<?php echo $form_create->scripts; ?>


</head>

<body class='<?php echo $page->name; ?> <?php echo $page->template; ?>'>

  <div class='page-wrapper'>
    <div id='header'></div>
    <div id='menu'></div>
    <div class='wrapper_inner'>
      <div id='home' pw-optional></div>
      <div id='created' pw-optional></div>
    </div>
  </div>

<!-- SCRIPT -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>

  <?php if($page->template == 'room_create'): ?>
    <script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/create.js"></script>
  <?php endif; ?>

  <?php if($page->template == 'rooms_put'): ?>
    <script type="text/javascript" src="<?php echo $config->urls->templates; ?>scripts/rooms.js"></script>
  <?php endif; ?>

<!-- /SCRIPT -->


</body>
</html>
