<?php namespace ProcessWire;

$rooms = $pages->get('/rooms/')->children("sort=-created");
$menu = $pages->get('/')->children;

// Converts $title to Title Case, and returns the result.
function strtotitle($title)
  { // Our array of 'small words' which shouldn't be capitalised if // they aren't the first word. Add your own words to taste.
  $smallwordsarray = array( 'of','a','the','and','an','or','nor','but','is','if','then','else','when', 'at','from','by','on','off','for','in','out','over','to','into','with' );

  // Split the string into separate words
  $words = explode(' ', $title); foreach ($words as $key => $word)
    { // If this word is the first, or it's not one of our small words, capitalise it // with ucwords().
      if ($key == 0 or !in_array($word, $smallwordsarray)) $words[$key] = ucwords($word); } // Join the words back into a string
      $newtitle = implode(' ', $words);
      return $newtitle;
  }


/*
* Include subscribe & survey form
*
*/
$form_create = $forms->render('create_room');

/*
* Include schema structured data
*
*/
include_once('./schema/schema.php');



/*
* SEO tag definition / Schema image
*
*/

// $schemaImage = $pages->get('/')->image->width(1200)->httpUrl;

// switch($page->template)
// {
//     case 'home';
//         if($page->seo_title) {
//           $seo_title = $page->seo_title;
//         } else {
//           $seo_title = 'WeAlgo | Where Alter Algos Meet';
//         }
//         if($page->seo_description) {
//           $seo_description = $page->seo_description;
//         } else {
//           $seo_description = "WeAlgo uses face recognition AI to protect our screen privacy, instead of violating it.";
//         }
//         // if($page->seo_canonical) {
//         //   $seo_canonical = $page->seo_canonical;
//         // } else {
//           // $seo_canonical = $_SERVER['HTTP_HOST'];
//           $seo_canonical = "https://" . $config->httpHost;
//         // }
//         // disallow index/follow if not live site
//         // if($config->httpHost == "aanjagers.com") {
//           $seo_robots = 'index,follow';
//         // } if ($page->seo_robots) {
//         //   $seo_robots = $page->seo_robots;
//         // } else {
//         //   $seo_robots = 'noindex,nofollow';
//         // }
//         $seo_keywords = "face recognition, video conferencing, machine learning, avatar, biometric, non biological entities, research, machine vision reality, visual experiment";
//         $seo_image = $page->og_image->width(1200)->httpUrl;
//     break;
//     default;
//         $seo_title = 'WeAlgo | ' . $page->title ;
//         $seo_description = "WeAlgo uses face recognition AI to protect our screen privacy, instead of violating it.";
//         $seo_keywords = "face recognition, video conferencing, machine learning, avatar, biometric, non biological entities, research, machine vision reality, visual experiment";
//         $seo_canonical = "https://" . $config->httpHost;
//         $seo_robots = 'index,follow';
//         $seo_image = $pages->get('/')->og_image->width(1200)->httpUrl;
//     break;
// }
//
//   /*
//   * SEO container for _main.php & _main_landing.php
//   *
//   */
//     $seo_container = '';
//     $seo_container .= "<title>$seo_title</title>";
//     $seo_container .= "<meta name='description' content='$seo_description'>";
//     $seo_container .= "<meta name='keywords' content='$seo_keywords'>";
//     $seo_container .= "<meta name='robots' content='$seo_robots'>";
//     $seo_container .= "<meta name='thumbnail' content='$seo_image'>";
//     $seo_container .= "<link rel='canonical' href='$seo_canonical'>";
//     $seo_container .= "<meta property='og:title' content='$seo_title'>";
//     $seo_container .= "<meta property='og:url' content='$seo_canonical'>";
//     $seo_container .= "<meta property='og:type' content='website'>";
//     $seo_container .= "<meta property='og:site_name' content='wealgo.org'>";
//     $seo_container .= "<meta property='og:description' content='$seo_description'>";
//     $seo_container .= "<meta property='og:image' itemprop='image' content='$seo_image'>";
//     $seo_container .= "<meta property='og:image:secure_url' itemprop='image' content='$seo_image'>";
//     $seo_container .= "<meta property='og:image:height' content='822'>";
//     $seo_container .= "<meta property='og:image:width' content='1200'>";
//     $seo_container .= "<meta name='twitter:card' content='$seo_description'>";
//     $seo_container .= "<meta name='twitter:title' content='$seo_title'>";
//     $seo_container .= "<meta name='twitter:description' content='$seo_description'>";
//     $seo_container .= "<meta name='twitter:url' content='$seo_canonical'>";
//     $seo_container .= "<meta property='twitter:image' content='$seo_image'>";
