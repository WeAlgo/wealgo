<?php namespace ProcessWire;
  include('./_header.php');
  include('./_menu.php');
?>


<div id='home'>

  <div class='create_wrapper'>

    <div class='create_inner'>


      <?php

      // no url segment; show form
      if(!$input->urlSegment) {
        // set form -->
        echo $form_create;
      }

      // form action; url segments
      if($input->urlSegment1 == 'created') {
        include('./room_created.php');
      }

      ?>

    </div>

  </div>


</div>
